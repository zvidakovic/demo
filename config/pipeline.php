<?php

declare(strict_types=1);

use Common\V1\Middleware\SessionLanguageMiddleware;
use Laminas\Stratigility\Handler\NotFoundHandler;
use Mezzio\Application;
use Mezzio\Helper\BodyParams\BodyParamsMiddleware;
use Mezzio\Helper\ServerUrlMiddleware;
use Mezzio\Helper\UrlHelperMiddleware;
use Mezzio\MiddlewareFactory;
use Mezzio\ProblemDetails\ProblemDetailsMiddleware;
use Mezzio\ProblemDetails\ProblemDetailsNotFoundHandler;
use Mezzio\Router\Middleware\DispatchMiddleware;
use Mezzio\Router\Middleware\ImplicitHeadMiddleware;
use Mezzio\Router\Middleware\ImplicitOptionsMiddleware;
use Mezzio\Router\Middleware\MethodNotAllowedMiddleware;
use Mezzio\Router\Middleware\RouteMiddleware;
use Mezzio\Session\SessionMiddleware;
use Psr\Container\ContainerInterface;

return static function (
    Application $application,
    MiddlewareFactory $middlewareFactory,
    ContainerInterface $container
): void {
    $application->pipe(ProblemDetailsMiddleware::class);
    $application->pipe(ServerUrlMiddleware::class);
    $application->pipe(RouteMiddleware::class);
    $application->pipe(ImplicitHeadMiddleware::class);
    $application->pipe(ImplicitOptionsMiddleware::class);
    $application->pipe(MethodNotAllowedMiddleware::class);
    $application->pipe(UrlHelperMiddleware::class);
    $application->pipe(SessionMiddleware::class);
    $application->pipe(SessionLanguageMiddleware::class);
    $application->pipe(BodyParamsMiddleware::class);
    $application->pipe(DispatchMiddleware::class);
    $application->pipe(ProblemDetailsNotFoundHandler::class);
    $application->pipe(NotFoundHandler::class);
};
