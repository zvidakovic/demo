<?php

declare(strict_types=1);

use Enqueue\AmqpBunny\AmqpConnectionFactory;

return [
    AmqpConnectionFactory::class => [
        'host' => 'rabbitmq',
        'port' => 5672,
        'vhost' => '/',
        'user' => 'rabbitmq',
        'pass' => 'rabbitmq',
        'persisted' => false,
    ],
];
