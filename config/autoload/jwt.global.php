<?php

declare(strict_types=1);

use Firebase\JWT\JWT;

return [
    JWT::class => [
        'privateKeyFile' => $_ENV['APP_BASE_PATH'] . '/jwt.RS256.key',
        'algo' => 'RS256',
    ],
];
