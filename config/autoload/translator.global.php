<?php

declare(strict_types=1);

use Common\V1\Middleware\SessionLanguageMiddleware;
use Laminas\I18n\Translator\Loader\PhpArray;

return [
    SessionLanguageMiddleware::class => [
        'default_locales' => 'en_US',
        'available_locales' => [
            'en_US',
            'de_DE',
        ],
    ],
    'translator' => [
        'locale' => 'en_US',
        'translation_file_patterns' => [
            [
                'type' => PhpArray::class,
                'base_dir' => $_ENV['APP_BASE_DATA_PATH'] . '/src/Common/i18n',
                'pattern' => '%s.php',
            ],
        ],
    ],
];
