<?php

declare(strict_types=1);

use Laminas\Cache\Storage\Adapter\Filesystem;

return [
    Filesystem::class => [
        'cache_dir' => $_ENV['APP_BASE_CACHE_PATH'],
        'ttl' => (int)$_ENV['APP_CACHE_TTL'],
    ],
];
