<?php

declare(strict_types=1);

use Common\V1\Doctrine\DBAL\Types\EnumAddressType;
use Common\V1\Doctrine\DBAL\Types\EnumNetworkSourceType;
use Common\V1\Doctrine\DBAL\Types\EnumNetworkType;
use Common\V1\Doctrine\DBAL\Types\EnumNotificationType;
use Common\V1\Doctrine\DBAL\Types\EnumStreamStatusType;
use Common\V1\Doctrine\DBAL\Types\EnumUserRoleType;
use Common\V1\Doctrine\DBAL\Types\EnumUserStatusType;
use Common\V1\Doctrine\DBAL\Types\EnumUserTypeType;
use Doctrine\DBAL\Driver\PDO\PgSQL\Driver;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Ramsey\Uuid\Doctrine\UuidType;

return [
    'doctrine' => [
        'configuration' => [
            'orm_default' => [
                'types' => [
                    UuidType::NAME => UuidType::class,
                    EnumAddressType::NAME => EnumAddressType::class,
                    EnumUserTypeType::NAME => EnumUserTypeType::class,
                    EnumUserStatusType::NAME => EnumUserStatusType::class,
                    EnumNotificationType::NAME => EnumNotificationType::class,
                    EnumStreamStatusType::NAME => EnumStreamStatusType::class,
                    EnumNetworkType::NAME => EnumNetworkType::class,
                    EnumNetworkSourceType::NAME => EnumNetworkSourceType::class,
                    EnumUserRoleType::NAME => EnumUserRoleType::class,
                ],
            ],
        ],
        'connection' => [
            'orm_default' => [
                'driverClass' => Driver::class,
                'params' => [
                    'user' => (string)$_ENV['APP_DB_USER'],
                    'password' => (string)$_ENV['APP_DB_PASS'],
                    'dbname' => (string)$_ENV['APP_DB_NAME'],
                    'host' => (string)$_ENV['APP_DB_HOST'],
                    'port' => (int)$_ENV['APP_DB_PORT'],
                ],
            ],
        ],
        'driver' => [
            'orm_default' => [
                'class' => AnnotationDriver::class,
                'paths' => [
                    $_ENV['APP_BASE_PATH'] . '/src/Common/src/V1/Entity',
                ],
            ],
        ],
    ],
];
