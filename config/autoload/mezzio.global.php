<?php

declare(strict_types=1);

use Laminas\ConfigAggregator\ConfigAggregator;

return [
    ConfigAggregator::ENABLE_CACHE => (bool)$_ENV['APP_CACHE_ENABLED'],
    'debug' => (bool)$_ENV['APP_DEBUG'],
];
