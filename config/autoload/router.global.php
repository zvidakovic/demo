<?php

declare(strict_types=1);

return [
    'router' => [
        'fastroute' => [
            'cache_enabled' => (bool)$_ENV['APP_CACHE_ENABLED'],
            'cache_file' => $_ENV['APP_BASE_CACHE_PATH'] . '/fastroute.php.cache',
        ],
        'routes' => [],
    ],
];
