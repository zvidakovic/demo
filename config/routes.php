<?php

declare(strict_types=1);

use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;

return static function (
    Application $application,
    MiddlewareFactory $middlewareFactory,
    ContainerInterface $container
): void {
    /** @var array[] $config */
    $config = $container->get('config');

    foreach ($config['router']['routes'] as $route) {
        if (isset($route['path'], $route['middleware'], $route['methods'], $route['name'])) {
            $application->route(
                (string)$route['path'],
                (array)$route['middleware'],
                (array)$route['methods'],
                (string)$route['name']
            );
        }
    }
};
