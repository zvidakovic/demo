<?php

declare(strict_types=1);

use Laminas\ConfigAggregator\ArrayProvider;
use Laminas\ConfigAggregator\ConfigAggregator;
use Laminas\ConfigAggregator\PhpFileProvider;

$cacheConfig = [
    'config_cache_path' => $_ENV['APP_BASE_CACHE_PATH'] . '/config-cache.php',
];

$aggregator = new ConfigAggregator(
    [
        \Laminas\Mail\ConfigProvider::class,
        \Mezzio\ProblemDetails\ConfigProvider::class,
        \Mezzio\Hal\ConfigProvider::class,
        \Laminas\Serializer\ConfigProvider::class,
        \DoctrineORMModule\ConfigProvider::class,
        \DoctrineModule\ConfigProvider::class,
        \Laminas\Form\ConfigProvider::class,
        \Laminas\InputFilter\ConfigProvider::class,
        \Laminas\Filter\ConfigProvider::class,
        \Laminas\Hydrator\ConfigProvider::class,
        \Laminas\Router\ConfigProvider::class,
        \Laminas\Cache\ConfigProvider::class,
        \Laminas\I18n\ConfigProvider::class,
        \Laminas\Paginator\ConfigProvider::class,
        \Laminas\Validator\ConfigProvider::class,
        \Mezzio\Session\Ext\ConfigProvider::class,
        \Mezzio\Session\ConfigProvider::class,
        \Laminas\Cache\ConfigProvider::class,
        \Laminas\I18n\ConfigProvider::class,
        \Mezzio\Router\FastRouteRouter\ConfigProvider::class,
        \Mezzio\Helper\ConfigProvider::class,
        \Mezzio\Hal\ConfigProvider::class,
        \Mezzio\ConfigProvider::class,
        \Mezzio\Router\ConfigProvider::class,
        \Laminas\Diactoros\ConfigProvider::class,
        \DoctrineORMModule\ConfigProvider::class,
        \Common\V1\ConfigProvider::class,
        \Event\V1\ConfigProvider::class,
        \Task\V1\ConfigProvider::class,
        \Api\V1\Common\ConfigProvider::class,
        \Api\V1\User\ConfigProvider::class,
        new ArrayProvider($cacheConfig),
        new PhpFileProvider(realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php'),
    ],
    $cacheConfig['config_cache_path']
);

return $aggregator->getMergedConfig();
