# SKELETON

**THIS IS SAMPLE SHOWCASE/DEMO APP.**

**MAKE SURE TO UPDATE [config/.env](config/.env) AND [config/.env.local](config/.env.local).**

- Zend/Laminas framework
- Docker
- Ngnix
- Postgres
- RabbitMQ
- Clean code
- Clear business logic responsibility division
- Sample handlers, event and task
- Form/input handling
- JSON API with HAL
- JWT Token handling
- Programming patterns
    - Delegator
    - Factory
    - Chain of responsibility
    - Observer
    - Events
    - Singleton
    - Builder
    - Command
    - Strategy
    - Hydration
- Dataflow

## RUNNING THE APP

**Please, make sure your Docker permissions are correctly set.**


```bash
docker-compose up -d
dokcer-compose exec php ash
#INSIDE CONTAINER
composer i -n
#INITIALLY, YOU WILL NEED KEY PAR FOR JWT
composer run generate-jwt-key
#CREATE DATABASE FROM ENTITIES
php bin/doctrine orm:schema-tool:drop --force && php bin/doctrine orm:schema-tool:create
#GENERATE SOME SAMPLE USERS
php /var/www/bin/console generate:data
```

Docker should expose following services on your local machine:

- Ngnix [http://127.0.0.1:8080](http://127.0.0.1:8080)
- RabbitMQ [http://127.0.0.1:15672](http://127.0.0.1:15672)
- Postgres [http://127.0.0.1:5432](http://127.0.0.1:5432)

## MODULES

### General module structure

**NOTE:** Naming convention, versioning and asset placement!

**ALL** services/handlers must follow some known programming pattern.

```
src/Module
  src
    V1
      Common
        [STUFF SHARED BETWEEN SUBMODULES, IF ANY]
      Service
        SomeService
          Factory.php
        SomeService.php
      Handler
        SomeHandler
          Factory.php
        SomeHandler.php
      ConfigProvider.php
    [V2]
    [V3]
    ...
  i18n
  img
  js
  [ASSETS]
  ...
```

### Common

Hold all classes used between other modules.

### Api

Holds all API related classes. API endpoints should not do
more than **one** task. All other accompanied tasks should
be done via `Event -> [GENERATE TASK] -> run:task` chain.

### Event

Holds all Event related classes. Event module is responsible
for generation of Tasks. It hooks on to events and generate Tasks
for RabbitMQ queue service.

### Tasks

Holds all Tasks that will be ran by cron. All tasks should do **ONE** thing
only and should be wrapped in `Amp\Parallel\Worker\Task` so we can run them in
parallel. Cronjob will take Tasks from RabbitMQ and run them.

Command should be used to control speed and volume of executing tasks.
