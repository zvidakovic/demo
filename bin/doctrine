#!/usr/bin/env php
<?php

declare(strict_types=1);

use Common\V1\Service\LoggerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;
use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__) . '/vendor/autoload.php';
(new Dotenv())->loadEnv(dirname(__DIR__) . '/config/.env');

try {
    /** @var ContainerInterface $container */
    $container = require dirname(__DIR__) . '/config/container.php';

    /** @var Application $application */
    $application = $container->get(Application::class);

    /** @var MiddlewareFactory $middlewareFactory */
    $middlewareFactory = $container->get(MiddlewareFactory::class);

    (require dirname(__DIR__) . '/config/pipeline.php')($application, $middlewareFactory, $container);
    (require dirname(__DIR__) . '/config/routes.php')($application, $middlewareFactory, $container);

    /** @var EntityManager $entityManager */
    $entityManager = $container->get(EntityManager::class);

    ConsoleRunner::run(ConsoleRunner::createHelperSet($entityManager), []);
} catch (Throwable $error) {
    LoggerService::log()->error($error->getMessage());
    exit(1);
}
