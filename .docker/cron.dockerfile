FROM app/php:7.4

USER root

RUN rm -f /etc/crontabs/root &&\
    touch /etc/crontabs/root &&\
    chown -R www-data:www-data /var/www &&\
    chmod -R 0774 /var/www

CMD ["sh", "-c", "/usr/bin/crontab -u www-data /var/www/crontab && /usr/sbin/crond -f -L /proc/self/fd/1"]
