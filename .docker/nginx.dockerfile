FROM nginx:1.15.5

COPY .docker/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443
