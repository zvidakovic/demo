FROM php:7.4-fpm-alpine

###########################################################
#PRODUCTION ENVIRONMENT
###########################################################

ENV PHP_FPM_USER="www-data" \
    PHP_FPM_GROUP="www-data" \
    TZ="Europe/Berlin"

#TIMEZONE
RUN apk add --no-cache --virtual .deps tzdata  &&\
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime &&\
    echo $TZ > /etc/timezone &&\
    apk del .deps

#NAMESERVERS
RUN echo "nameserver 1.1.1.1" >> /etc/resolv.conf &&\
    echo "nameserver 1.0.0.1" >> /etc/resolv.conf &&\
    echo "nameserver 8.8.8.8" >> /etc/resolv.conf &&\
    echo "nameserver 8.8.4.4" >> /etc/resolv.conf

#DEPENDENCIES
RUN apk add --no-cache --virtual .deps \
        autoconf \
        build-base \
        curl-dev \
        zlib-dev \
        icu-dev \
        postgresql-dev \
        libxml2-dev \
        libzip-dev

#INSTALL EXTENSIONS
RUN docker-php-ext-enable sodium &&\
    docker-php-ext-install -j$(nproc) pdo &&\
    docker-php-ext-install -j$(nproc) dom &&\
    docker-php-ext-install -j$(nproc) json &&\
    docker-php-ext-install -j$(nproc) mysqli &&\
    docker-php-ext-install -j$(nproc) fileinfo &&\
    docker-php-ext-install -j$(nproc) intl &&\
    docker-php-ext-install -j$(nproc) pdo_pgsql &&\
    docker-php-ext-install -j$(nproc) zip

#PHP.INI SETTINGS
RUN echo "date.timezone=Europe/Berlin" >> "$PHP_INI_DIR"/php.ini-production &&\
    echo "memory_limit=2048M"          >> "$PHP_INI_DIR"/php.ini-production &&\
    echo "date.timezone=Europe/Berlin" >> "$PHP_INI_DIR"/php.ini-development &&\
    echo "memory_limit=2048M"          >> "$PHP_INI_DIR"/php.ini-development &&\
    cp "$PHP_INI_DIR"/php.ini-production  "$PHP_INI_DIR"/php.ini &&\
    echo "php_flag[display_errors]=off"                >> /usr/local/etc/php-fpm.conf &&\
    echo "php_admin_flag[log_errors]=on"               >> /usr/local/etc/php-fpm.conf &&\
    echo "php_admin_value[error_log]=/proc/self/fd/2"  >> /usr/local/etc/php-fpm.conf &&\
    echo "php_admin_value[error_reporting]=E_ALL"      >> /usr/local/etc/php-fpm.conf &&\
    echo "php_admin_value[display_startup_errors]=off" >> /usr/local/etc/php-fpm.conf

#PROD
RUN apk del .deps &&\
    apk add --no-cache curl \
                       zlib \
                       icu \
                       libzip \
                       libpq \
                       libxml2 \
                       openssh-keygen

###########################################################
#DEVELOPMENT ENVIRONMENT
###########################################################

#XDEBUG
ENV EXT_XDEBUG_VERSION=2.9.5
RUN docker-php-source extract &&\
    mkdir -p /usr/src/php/ext/xdebug &&\
    curl -fsSL https://github.com/xdebug/xdebug/archive/$EXT_XDEBUG_VERSION.tar.gz | tar xvz -C /usr/src/php/ext/xdebug --strip 1 &&\
    docker-php-ext-configure xdebug &&\
    docker-php-ext-install xdebug &&\
    docker-php-source delete &&\
    echo "xdebug.default_enable=0"                 >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.remote_enable=1"                  >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.remote_autostart=1"               >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.remote_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.remote_port=9000"                 >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.remote_connect_back=1"            >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    echo "xdebug.profiler_enable=0"                >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini &&\
    cp "$PHP_INI_DIR"/php.ini-development                "$PHP_INI_DIR"/php.ini &&\
    echo "php_flag[display_errors]=on"                >> /usr/local/etc/php-fpm.conf &&\
    echo "php_admin_value[display_startup_errors]=on" >> /usr/local/etc/php-fpm.conf

#WORKDIR
RUN mkdir -p /var/www &&\
    chown -R $PHP_FPM_USER:$PHP_FPM_GROUP /var/www /home/$PHP_FPM_USER &&\
    chmod -R 0774 /var/www

#COMPOSER
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" &&\
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'c31c1e292ad7be5f49291169c0ac8f683499edddcfd4e42232982d0fd193004208a58ff6f353fde0012d35fdd72bc394') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" &&\
    php composer-setup.php &&\
    php -r "unlink('composer-setup.php');" &&\
    mv composer.phar /usr/local/bin/composer
ENV COMPOSER_HOME="/home/$PHP_FPM_USER/.composer" COMPOSER_MEMORY_LIMIT="-1"

#CLEAN UP
RUN rm -rf /var/cache/apk/*

WORKDIR /var/www

USER $PHP_FPM_USER

EXPOSE 9000
