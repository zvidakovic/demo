<?php

declare(strict_types=1);

namespace Common\V1\Command;

use Common\V1\Doctrine\DBAL\Types\EnumUserStatusType;
use Common\V1\Doctrine\DBAL\Types\EnumUserTypeType;
use Common\V1\Entity\User;
use Common\V1\Service\UuidService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Faker\Generator;
use Laminas\Crypt\Password\BcryptSha;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateDataCommand extends Command
{
    protected static $defaultName = 'generate:data';

    private UuidService $uuidService;
    private EntityManager $entityManager;
    private Generator $faker;

    public function __construct(
        UuidService $uuidService,
        EntityManager $entityManager,
        Generator $faker
    ) {
        parent::__construct(self::$defaultName);
        $this->uuidService = $uuidService;
        $this->entityManager = $entityManager;
        $this->faker = $faker;
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->generateUsers();
        return 0;
    }

    /**
     * @param int $count
     * @return User[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function generateUsers(int $count = 40): array
    {
        $collection = [];
        $crypt = new BcryptSha();

        for ($i = 0; $i < $count; $i++) {
            $entity = new User();

            $entity->setFirstName($this->faker->firstName);
            $entity->setLastName($this->faker->name);
            $entity->setEmail($this->faker->email);
            $entity->setPublicUuid($this->uuidService->generateUuid($entity->getEmail()));
            $entity->setPasswordHash($crypt->create('fake123'));
            $entity->setAccountTypeId((int)array_rand(EnumUserTypeType::$valueLabelMap));
            $entity->setStatusId((int)array_rand(EnumUserStatusType::$valueLabelMap));
            $entity->setDateCreated(new DateTimeImmutable());
            $entity->setDateUpdated(new DateTimeImmutable());

            $this->entityManager->persist($entity);

            $collection[] = $entity;
        }

        $this->entityManager->flush();

        return $collection;
    }
}
