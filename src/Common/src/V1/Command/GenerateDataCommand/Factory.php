<?php

declare(strict_types=1);

namespace Common\V1\Command\GenerateDataCommand;

use Common\V1\Command\GenerateDataCommand;
use Common\V1\Service\UuidService;
use Doctrine\ORM\EntityManager;
use Faker\Generator;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var UuidService $uuidService */
        $uuidService = $container->get(UuidService::class);
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var Generator $faker */
        $faker = \Faker\Factory::create();

        return new GenerateDataCommand(
            $uuidService,
            $entityManager,
            $faker
        );
    }
}
