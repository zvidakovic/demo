<?php

declare(strict_types=1);

namespace Common\V1\Hydrator\UserHydrator;

use Common\V1\Hydrator\UserHydrator;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);

        return new UserHydrator(
            $entityManager,
        );
    }
}
