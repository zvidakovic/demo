<?php

declare(strict_types=1);

namespace Common\V1\Service;

use Common\V1\Entity\Post;
use GuzzleHttp\Client;

final class TwitterService
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array<Post>
     */
    public function getByTags(): array
    {
        return [];
    }
}
