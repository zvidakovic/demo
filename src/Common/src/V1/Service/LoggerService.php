<?php

declare(strict_types=1);

namespace Common\V1\Service;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use Psr\Log\LoggerInterface;

final class LoggerService
{
    private static LoggerInterface $logger;

    public function debug(string $message, array $context = []): void
    {
        self::log()->debug($message, $context);
    }

    public static function log(): LoggerInterface
    {
        if (isset(self::$logger)) {
            return self::$logger;
        }

        self::$logger = new Logger($_ENV['APP_ENV']);

        if ($_ENV['APP_ENV'] === 'dev') {
            self::$logger->pushHandler(new StreamHandler(STDOUT));
        }

        self::$logger->pushHandler(new RotatingFileHandler(sprintf('%s/log.log', $_ENV['APP_BASE_LOG_PATH'])));
        self::$logger->pushProcessor(new PsrLogMessageProcessor());

        return self::$logger;
    }

    public function alert(string $message, array $context = []): void
    {
        self::log()->alert($message, $context);
    }

    public function emergency(string $message, array $context = []): void
    {
        self::log()->emergency($message, $context);
    }

    public function info(string $message, array $context = []): void
    {
        self::log()->info($message, $context);
    }

    public function error(string $message, array $context = []): void
    {
        self::log()->error($message, $context);
    }

    public function warning(string $message, array $context = []): void
    {
        self::log()->warning($message, $context);
    }

    public function notice(string $message, array $context = []): void
    {
        self::log()->notice($message, $context);
    }
}
