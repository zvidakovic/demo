<?php

declare(strict_types=1);

namespace Common\V1\Service\UuidService;

use Common\V1\Service\UuidService;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        return new UuidService();
    }
}
