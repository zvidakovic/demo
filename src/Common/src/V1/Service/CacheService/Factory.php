<?php

declare(strict_types=1);

namespace Common\V1\Service\CacheService;

use Laminas\Cache\Psr\SimpleCache\SimpleCacheDecorator;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Plugin\Serializer;
use Laminas\Cache\StorageFactory;
use Psr\Container\ContainerInterface;
use RuntimeException;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var array[] $config */
        $config = $container->get('config');

        if (empty($config[Filesystem::class])) {
            throw new RuntimeException('Cache configuration is not set.');
        }

        /** @var Filesystem $storageInterface */
        $storageInterface = StorageFactory::adapterFactory(Filesystem::class, $config[Filesystem::class]);
        $storageInterface->addPlugin(StorageFactory::pluginFactory(Serializer::class));

        return new SimpleCacheDecorator($storageInterface);
    }
}
