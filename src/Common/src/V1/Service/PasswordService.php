<?php

declare(strict_types=1);

namespace Common\V1\Service;

use Laminas\Crypt\Password\PasswordInterface;

final class PasswordService
{
    private PasswordInterface $password;

    public function __construct(
        PasswordInterface $password
    ) {
        $this->password = $password;
    }

    public function generatePassword(string $password): string
    {
        return $this->password->create($this->applySalt($password));
    }

    private function applySalt(string $password): string
    {
        return sprintf('%s.%s', $password, $_ENV['APP_SALT']);
    }

    public function verifyPassword(string $password, string $hash): bool
    {
        return $this->password->verify($this->applySalt($password), $hash);
    }
}
