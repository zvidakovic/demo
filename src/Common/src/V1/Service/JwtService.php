<?php

declare(strict_types=1);

namespace Common\V1\Service;

use Firebase\JWT\JWT;
use stdClass;

final class JwtService
{
    private JWT $jwt;
    private string $privateKey;
    private string $algo;

    public function __construct(
        JWT $jwt,
        string $privateKeyFile,
        string $algo
    ) {
        $this->jwt = $jwt;
        $this->privateKey = trim(file_get_contents($privateKeyFile));
        $this->algo = $algo;
    }

    public function encode(array $array): string
    {
        return $this->jwt::encode($array, $this->privateKey, $this->algo);
    }

    public function decode(string $jwt): stdClass
    {
        return $this->jwt::decode($jwt, $this->privateKey, [$this->algo]);
    }
}
