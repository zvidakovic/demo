<?php

declare(strict_types=1);

namespace Common\V1\Service\EventManagerService;

use Event\V1\Listener\UserCreatedListener;
use Laminas\EventManager\EventManager;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $eventManager = new EventManager();

        /** @var UserCreatedListener $sendRegisterEmailListener */
        $sendRegisterEmailListener = $container->get(UserCreatedListener::class);

        $sendRegisterEmailListener->setEventManager($eventManager);

        return $eventManager;
    }
}
