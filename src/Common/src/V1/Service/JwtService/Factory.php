<?php

declare(strict_types=1);

namespace Common\V1\Service\JwtService;

use Common\V1\Service\JwtService;
use Firebase\JWT\JWT;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var array[] $config */
        $config = $container->get('config');

        $privateKeyFile = (string)($config[JWT::class]['privateKeyFile'] ?? '');
        $algo = (string)($config[JWT::class]['algo'] ?? '');

        return new JwtService(
            new JWT(),
            $privateKeyFile,
            $algo
        );
    }
}
