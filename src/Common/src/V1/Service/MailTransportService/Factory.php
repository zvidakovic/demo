<?php

declare(strict_types=1);

namespace Common\V1\Service\MailTransportService;

use Laminas\Mail\Transport\InMemory;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        if ($_ENV['APP_ENV'] !== 'prod') {
            return new InMemory();
        }

        return new Smtp(
            new SmtpOptions(
                [
                    'name' => $_ENV['APP_SMTP_HOST'],
                    'host' => $_ENV['APP_SMTP_HOST'],
                    'port' => (int)$_ENV['APP_SMTP_PORT'],
                    'connection_class' => 'plain',
                    'connection_config' => [
                        'username' => $_ENV['APP_SMTP_USER'],
                        'password' => $_ENV['APP_SMTP_PASS'],
                        'ssl' => $_ENV['APP_SMTP_ENCRYPTION'],
                    ],
                ]
            )
        );
    }
}
