<?php

declare(strict_types=1);

namespace Common\V1\Service\FastRouteService;

use FastRoute\DataGenerator\MarkBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        return new RouteCollector(
            new Std(),
            new MarkBased()
        );
    }
}
