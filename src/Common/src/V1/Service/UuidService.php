<?php

declare(strict_types=1);

namespace Common\V1\Service;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UuidService
{
    public const FASTROUTE_UUID_REGEX = '[0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-[1-5]{1}[0-9A-Fa-f]{3}-[ABab89]{1}[0-9A-Fa-f]{3}-[0-9A-Fa-f]{12}';

    public function generateUuid(string $seed): UuidInterface
    {
        return Uuid::uuid5(Uuid::NAMESPACE_DNS, sprintf('%s.%s', $seed, $_ENV['APP_SALT']));
    }
}
