<?php

declare(strict_types=1);

namespace Common\V1\Service\TwitterService;

use Common\V1\Service\TwitterService;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        $client = new Client(
            [
                'base_uri' => 'https://api.twitter.com/1.1',
                'headers' => [
                    'Authorization' => 'Bearer <BEARER_TOKEN>',
                    'Content-Type' => 'application/json',
                ],
            ]
        );

        return new TwitterService(
            $client
        );
    }
}
