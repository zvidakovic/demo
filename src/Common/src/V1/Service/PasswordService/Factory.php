<?php

declare(strict_types=1);

namespace Common\V1\Service\PasswordService;

use Common\V1\Service\PasswordService;
use Laminas\Crypt\Password\BcryptSha;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        return new PasswordService(
            new BcryptSha()
        );
    }
}
