<?php

declare(strict_types=1);

namespace Common\V1\Middleware;

use Laminas\Diactoros\Stream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\SimpleCache\CacheInterface;

final class CacheMiddleware implements MiddlewareInterface
{
    private CacheInterface $cache;
    private bool $cacheEnabled;

    public function __construct(
        CacheInterface $cache,
        bool $cacheEnabled
    ) {
        $this->cache = $cache;
        $this->cacheEnabled = $cacheEnabled;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!$this->cacheEnabled) {
            return $handler->handle($request);
        }

        $responseKey = $this->getKey($request);
        $bodyKey = "{$responseKey}-body";

        if ($this->cache->has($responseKey) && $this->cache->has($bodyKey)) {
            /** @var ResponseInterface $response */
            $response = $this->cache->get($responseKey);
            $stream = new Stream('php://temp', 'rw+');
            $stream->write($this->cache->get($bodyKey));
            $stream->rewind();
            return $response->withBody($stream);
        }

        $response = $handler->handle($request);

        $this->cache->setMultiple(
            [
                $responseKey => $response,
                $bodyKey => $response->getBody()->getContents(),
            ]
        );

        return $response;
    }

    private function getKey(ServerRequestInterface $request): string
    {
        $queryParams = (array)parse_url($request->getUri()->getQuery());
        ksort($queryParams);

        $keyParts = [
            $request->getUri()->getHost(),
            $request->getUri()->getPath(),
            http_build_query($queryParams),
        ];

        return md5(implode('-', $keyParts));
    }
}
