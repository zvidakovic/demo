<?php

declare(strict_types=1);

namespace Common\V1\Middleware\SessionLanguageMiddleware;

use Common\V1\Middleware\SessionLanguageMiddleware;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var array[] $config */
        $config = $container->get('config');

        return new SessionLanguageMiddleware(
            $config[SessionLanguageMiddleware::class]
        );
    }
}
