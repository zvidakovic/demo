<?php

declare(strict_types=1);

namespace Common\V1\Middleware;

use Locale;
use Mezzio\Session\SessionInterface;
use Mezzio\Session\SessionMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class SessionLanguageMiddleware implements MiddlewareInterface
{
    /** @var array[] $config */
    private array $config;

    /**
     * @param array[] $config
     */
    public function __construct(
        array $config
    ) {
        $this->config = $config;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        /** @var array<string> $params */
        $params = $request->getQueryParams();

        /** @var SessionInterface $session */
        $session = $request->getAttribute(SessionMiddleware::SESSION_ATTRIBUTE);

        $locale = $params['locale'] ?? null;

        if ($locale === null) {
            $locale = $this->getFromSession($session);
        }

        if ($locale === null) {
            $locale = $this->getFromHeader($request);
        }

        if ($locale === null || !in_array($locale, $this->config['available_locales'], true)) {
            $locale = $this->config['default_locales'];
        }

        $session->set('locale', $locale);

        return $handler->handle($request->withAttribute('locale', $locale));
    }

    private function getFromSession(SessionInterface $session): ?string
    {
        return $session->get('locale', null);
    }

    private function getFromHeader(ServerRequestInterface $request): ?string
    {
        $serverParams = $request->getServerParams();

        if (isset($serverParams['HTTP_ACCEPT_LANGUAGE'])) {
            $firstBrowserLocale = (string)Locale::acceptFromHttp($serverParams['HTTP_ACCEPT_LANGUAGE']);
            if (in_array($firstBrowserLocale, $this->config['available_locales'], true)) {
                return $firstBrowserLocale;
            }
        }

        return null;
    }
}
