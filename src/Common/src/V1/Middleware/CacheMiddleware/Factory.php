<?php

declare(strict_types=1);

namespace Common\V1\Middleware\CacheMiddleware;

use Common\V1\Middleware\CacheMiddleware;
use Laminas\ConfigAggregator\ConfigAggregator;
use Psr\Container\ContainerInterface;
use Psr\SimpleCache\CacheInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var CacheInterface $cache */
        $cache = $container->get(CacheInterface::class);

        /** @var array[] $config */
        $config = $container->get('config');

        $cacheEnabled = (bool)($config[ConfigAggregator::ENABLE_CACHE] ?? false);

        return new CacheMiddleware(
            $cache,
            $cacheEnabled
        );
    }
}
