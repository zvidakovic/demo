<?php

declare(strict_types=1);

namespace Common\V1\Hal\ResourceGenerator;

use Common\V1\Entity\User;
use Mezzio\Hal\HalResource;
use Mezzio\Hal\Metadata;
use Mezzio\Hal\ResourceGenerator;
use Mezzio\Hal\ResourceGenerator\RouteBasedResourceStrategy;
use Psr\Http\Message\ServerRequestInterface;

final class UserResourceStrategy extends RouteBasedResourceStrategy
{
    public function createResource(
        $instance,
        Metadata\AbstractMetadata $metadata,
        ResourceGenerator $resourceGenerator,
        ServerRequestInterface $request
    ): HalResource {
        /** @var User $instance */
        $halResource = parent::createResource($instance, $metadata, $resourceGenerator, $request);

        return $halResource->withLink(
            $resourceGenerator->getLinkGenerator()->fromRoute(
                'delete',
                $request,
                'api.v1.user.delete',
                ['uuid' => $instance->getUuid()->toString()]
            )
        );
    }
}
