<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use Common\V1\Doctrine\DBAL\Types\EnumStreamStatusType;
use DateTimeImmutable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("stream")
 */
class Stream
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $publicUuid;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="streams")
     * @ORM\JoinColumn(name="userUuid", referencedColumnName="uuid")
     */
    protected User $user;

    /**
     * @ORM\Column(type="enum_stream_status_type")
     */
    protected int $statusId = EnumStreamStatusType::STATUS_NEW;

    /**
     * @ORM\Column(type="string")
     */
    protected string $label;

    /**
     * @ORM\Column(type="text")
     */
    protected string $description = '';

    /**
     * @var Collection<Source>
     * @ORM\OneToMany(targetEntity="Source",mappedBy="stream")
     */
    protected Collection $sources;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateUpdated;
}
