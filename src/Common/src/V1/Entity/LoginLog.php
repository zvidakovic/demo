<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("loginlog")
 */
class LoginLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="loginLogs")
     * @ORM\JoinColumn(name="userUuid", referencedColumnName="uuid")
     */
    protected User $user;

    /**
     * @ORM\Column(type="string",length=46)
     */
    protected string $ip;

    /**
     * @ORM\Column(type="string")
     */
    protected string $userAgent;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;
}
