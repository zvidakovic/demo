<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use Common\V1\Doctrine\DBAL\Types\EnumNetworkSourceType;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("source")
 */
class Source
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $publicUuid;

    /**
     * @var Stream
     * @ORM\ManyToOne(targetEntity="Stream", inversedBy="sources")
     * @ORM\JoinColumn(name="streamUuid", referencedColumnName="uuid")
     */
    protected Stream $stream;

    /**
     * @ORM\Column(type="enum_network_source_type")
     */
    protected int $networkSourceTypeId = EnumNetworkSourceType::SOURCE_TWITTER_TAG;

    /**
     * @ORM\Column(type="string")
     */
    protected string $value;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateUpdated;
}
