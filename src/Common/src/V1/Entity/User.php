<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use Common\V1\Doctrine\DBAL\Types\EnumUserRoleType;
use Common\V1\Doctrine\DBAL\Types\EnumUserStatusType;
use Common\V1\Doctrine\DBAL\Types\EnumUserTypeType;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @ORM\Column(type="uuid",unique=true)
     */
    protected UuidInterface $publicUuid;

    /**
     * @ORM\Column(type="string",unique=true,length=100)
     */
    protected string $email;

    /**
     * @ORM\Column(type="string",unique=true,length=64)
     */
    protected string $passwordHash;

    /**
     * @ORM\Column(type="string",length=50)
     */
    protected string $firstName;

    /**
     * @ORM\Column(type="string",length=50)
     */
    protected string $lastName;

    /**
     * @ORM\Column(type="string",length=5)
     */
    protected string $locale = 'en_US';

    /**
     * @ORM\Column(type="boolean")
     */
    protected bool $active = true;

    /**
     * @ORM\Column(type="enum_user_type_type")
     */
    protected int $accountTypeId = EnumUserTypeType::TYPE_FREE;

    /**
     * @ORM\Column(type="enum_user_status_type")
     */
    protected int $statusId = EnumUserStatusType::STATUS_NEW;

    /**
     * @ORM\Column(type="enum_user_role_type")
     */
    protected int $roleId = EnumUserRoleType::ROLE_ADMIN;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateUpdated;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;

    /**
     * @var Collection<Stream>
     * @ORM\OneToMany(targetEntity="Stream",mappedBy="user")
     */
    protected Collection $streams;

    /**
     * @var Collection<LoginLog>
     * @ORM\OneToMany(targetEntity="LoginLog",mappedBy="user")
     */
    protected Collection $loginLogs;

    /**
     * @var Collection<Notification>
     * @ORM\OneToMany(targetEntity="Notification",mappedBy="user")
     */
    protected Collection $notifications;

    public function __construct()
    {
        $this->streams = new ArrayCollection();
        $this->loginLogs = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    /**
     * @return UuidInterface
     */
    public function getPublicUuid(): UuidInterface
    {
        return $this->publicUuid;
    }

    /**
     * @param UuidInterface $publicUuid
     */
    public function setPublicUuid(UuidInterface $publicUuid): void
    {
        $this->publicUuid = $publicUuid;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash
     */
    public function setPasswordHash(string $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getAccountTypeId(): int
    {
        return $this->accountTypeId;
    }

    /**
     * @param int $accountTypeId
     */
    public function setAccountTypeId(int $accountTypeId): void
    {
        $this->accountTypeId = $accountTypeId;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->statusId;
    }

    /**
     * @param int $statusId
     */
    public function setStatusId(int $statusId): void
    {
        $this->statusId = $statusId;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->roleId = $roleId;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateUpdated(): DateTimeImmutable
    {
        return $this->dateUpdated;
    }

    /**
     * @param DateTimeImmutable $dateUpdated
     */
    public function setDateUpdated(DateTimeImmutable $dateUpdated): void
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getDateCreated(): DateTimeImmutable
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTimeImmutable $dateCreated
     */
    public function setDateCreated(DateTimeImmutable $dateCreated): void
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return Collection
     */
    public function getStreams(): Collection
    {
        return $this->streams;
    }

    /**
     * @param Collection $streams
     */
    public function setStreams(Collection $streams): void
    {
        $this->streams = $streams;
    }

    /**
     * @return Collection
     */
    public function getLoginLogs(): Collection
    {
        return $this->loginLogs;
    }

    /**
     * @param Collection $loginLogs
     */
    public function setLoginLogs(Collection $loginLogs): void
    {
        $this->loginLogs = $loginLogs;
    }

    /**
     * @return Collection
     */
    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    /**
     * @param Collection $notifications
     */
    public function setNotifications(Collection $notifications): void
    {
        $this->notifications = $notifications;
    }
}
