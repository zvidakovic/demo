<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use Common\V1\Doctrine\DBAL\Types\EnumNetworkSourceType;
use Common\V1\Doctrine\DBAL\Types\EnumNetworkType;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("post")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $publicUuid;

    /**
     * @ORM\Column(type="enum_network_type")
     */
    protected int $networkId = EnumNetworkType::NETWORK_TWITTER;

    /**
     * @ORM\Column(type="string")
     */
    protected string $networkUserUrl = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $networkPostUrl = '';
    protected int $status;
    protected bool $flagged = false;
    protected bool $hasMedia = true;
    protected string $sourceIdentifier;

    /**
     * @ORM\Column(type="enum_network_source_type")
     */
    protected int $sourceTypeId = EnumNetworkSourceType::SOURCE_TWITTER_TAG;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $sourceDateCreated;

    /**
     * @ORM\Column(type="string")
     */
    protected string $userScreenName = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $userFullName = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $userImageUrl = '';

    /**
     * @ORM\Column(type="text")
     */
    protected string $text = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $imageUrl = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $videoUrl = '';

    /**
     * @ORM\Column(type="string")
     */
    protected string $thumbnailUrl = '';

    /**
     * @ORM\Column(type="smallint")
     */
    protected int $videoWidth = 0;

    /**
     * @ORM\Column(type="smallint")
     */
    protected int $videoHeight = 0;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateUpdated;
}
