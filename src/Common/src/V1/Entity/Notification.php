<?php

declare(strict_types=1);

namespace Common\V1\Entity;

use Common\V1\Doctrine\DBAL\Types\EnumNotificationType;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @ORM\Table("notification")
 */
class Notification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid",unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected UuidInterface $uuid;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notifications")
     * @ORM\JoinColumn(name="userUuid", referencedColumnName="uuid")
     */
    protected User $user;

    /**
     * @ORM\Column(type="enum_notification_type")
     */
    protected int $typeId = EnumNotificationType::TYPE_EMAIL;

    /**
     * @ORM\Column(type="string")
     */
    protected string $subject = '';

    /**
     * @ORM\Column(type="text")
     */
    protected string $body = '';

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateOfDelivery;

    /**
     * @ORM\Column(type="datetime_immutable",nullable=true)
     */
    protected ?DateTimeImmutable $dateDelivered;

    /**
     * @ORM\Column(type="datetime_immutable",nullable=true)
     */
    protected ?DateTimeImmutable $dateSeen;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    protected DateTimeImmutable $dateCreated;
}
