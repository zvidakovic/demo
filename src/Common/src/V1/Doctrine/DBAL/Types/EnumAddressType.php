<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumAddressType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_address_type';

    public const TYPE_PERSONAL = 10;
    public const TYPE_BUSINESS = 20;

    public static int $defaultValue = self::TYPE_PERSONAL;

    public static array $valueLabelMap = [
        self::TYPE_PERSONAL => 'Personal',
        self::TYPE_BUSINESS => 'Business',
    ];
}
