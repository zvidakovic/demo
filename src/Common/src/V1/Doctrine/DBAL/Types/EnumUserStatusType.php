<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumUserStatusType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_user_status_type';

    public const STATUS_NEW = 10;
    public const STATUS_CONFIRMED = 20;
    public const STATUS_UNDER_REVIEW = 30;
    public const STATUS_DISABLED = 40;
    public const STATUS_ARCHIVED = 50;
    public const STATUS_FLAGGED = 60;

    public static int $defaultValue = self::STATUS_NEW;

    public static array $valueLabelMap = [
        self::STATUS_NEW => 'New',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_UNDER_REVIEW => 'Under review',
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_ARCHIVED => 'Archived',
        self::STATUS_FLAGGED => 'Flagged',
    ];
}
