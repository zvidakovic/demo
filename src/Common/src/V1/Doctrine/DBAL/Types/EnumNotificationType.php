<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumNotificationType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_notification_type';

    public const TYPE_EMAIL = 10;
    public const TYPE_PUSH = 20;

    public static int $defaultValue = self::TYPE_EMAIL;

    public static array $valueLabelMap = [
        self::TYPE_EMAIL => 'Email',
        self::TYPE_PUSH => 'Push',
    ];
}
