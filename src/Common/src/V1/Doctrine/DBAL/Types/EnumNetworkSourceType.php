<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumNetworkSourceType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_network_source_type';

    public const SOURCE_TWITTER_TAG = 10;
    public const SOURCE_TWITTER_USER = 11;
    public const SOURCE_TWITTER_TIMELINE = 12;
    public const SOURCE_TWITTER_USERNAME_MENTION = 13;
    public const SOURCE_TWITTER_GEOLOCATION = 14;

    public const SOURCE_INSTAGRAM_BUSINESS = 20;
    public const SOURCE_INSTAGRAM_TIMELINE = 21;
    public const SOURCE_INSTAGRAM_TAG = 22;

    public const SOURCE_YOUTUBE_CHANNEL = 30;
    public const SOURCE_YOUTUBE_TAG = 31;
    public const SOURCE_YOUTUBE_PLAYLIST = 32;

    public const SOURCE_FACEBOOK_PAGE = 40;
    public const SOURCE_FACEBOOK_VISITOR_POSTS = 41;

    public const SOURCE_GOOGLE_TAG = 50;
    public const SOURCE_GOOGLE_USER = 51;

    public const SOURCE_PINTEREST_USER = 60;
    public const SOURCE_PINTEREST_BOARD = 61;
    public const SOURCE_PINTEREST_BOARD_SECTION = 62;

    public const SOURCE_FLICKR_TAG = 70;
    public const SOURCE_FLICKR_USER = 71;

    public const SOURCE_TUMBLR_TAG = 80;
    public const SOURCE_TUMBLR_USER = 81;

    public const SOURCE_RSS_URL = 90;

    public const SOURCE_VIMEO_TAG = 100;

    public const SOURCE_LINKED_IN_PAGE = 110;

    public const SOURCE_VINE_USER = 120;

    public const SOURCE_FLICKR_ALBUM = 130;

    public const SOURCE_DIFFBOT_CRAWLBOT = 140;

    public const SOURCE_GLASSDOOR_EMPLOYER = 150;

    public static int $defaultValue = self::SOURCE_TWITTER_USER;

    public static array $valueLabelMap = [
        self::SOURCE_TWITTER_TAG => 'Twitter Tag',
        self::SOURCE_TWITTER_USER => 'Twitter User',
        self::SOURCE_TWITTER_TIMELINE => 'Twitter Timeline',
        self::SOURCE_TWITTER_USERNAME_MENTION => 'Twitter username mention',
        self::SOURCE_TWITTER_GEOLOCATION => 'Twitter geolocation',
        self::SOURCE_INSTAGRAM_BUSINESS => 'Instagram business',
        self::SOURCE_INSTAGRAM_TIMELINE => 'Instagram Timeline',
        self::SOURCE_INSTAGRAM_TAG => 'Instagram Tag',
        self::SOURCE_YOUTUBE_CHANNEL => 'YouTube Channel',
        self::SOURCE_YOUTUBE_TAG => 'YouTube Tag',
        self::SOURCE_YOUTUBE_PLAYLIST => 'YouTube Playlist',
        self::SOURCE_FACEBOOK_PAGE => 'Facebook Page',
        self::SOURCE_FACEBOOK_VISITOR_POSTS => 'Facebook Visitor Posts',
        self::SOURCE_GOOGLE_TAG => 'Google+ Tag',
        self::SOURCE_GOOGLE_USER => 'Google+ User',
        self::SOURCE_PINTEREST_USER => '',
        self::SOURCE_PINTEREST_BOARD => '',
        self::SOURCE_PINTEREST_BOARD_SECTION => '',
        self::SOURCE_FLICKR_TAG => 'Flickr Tag',
        self::SOURCE_FLICKR_USER => 'Flickr User',
        self::SOURCE_TUMBLR_TAG => 'Tumble Tag',
        self::SOURCE_TUMBLR_USER => 'Tumblr User',
        self::SOURCE_RSS_URL => 'RSS URL',
        self::SOURCE_VIMEO_TAG => 'Vimeo Tag',
        self::SOURCE_LINKED_IN_PAGE => 'LinkedIn Page',
        self::SOURCE_VINE_USER => 'Vin User',
        self::SOURCE_FLICKR_ALBUM => 'Flickr Album',
        self::SOURCE_DIFFBOT_CRAWLBOT => 'Diffbot Crawlbot',
        self::SOURCE_GLASSDOOR_EMPLOYER => 'Glassdoor employer',
    ];

    public static array $networkSourceMap = [
        EnumNetworkType::NETWORK_TWITTER => [
            self::SOURCE_TWITTER_TAG,
            self::SOURCE_TWITTER_USER,
            self::SOURCE_TWITTER_TIMELINE,
            self::SOURCE_TWITTER_USERNAME_MENTION,
            self::SOURCE_TWITTER_GEOLOCATION,
        ],
        EnumNetworkType::NETWORK_INSTAGRAM => [
            self::SOURCE_INSTAGRAM_BUSINESS,
            self::SOURCE_INSTAGRAM_TIMELINE,
            self::SOURCE_INSTAGRAM_TAG,
        ],
        EnumNetworkType::NETWORK_YOUTUBE => [
            self::SOURCE_YOUTUBE_CHANNEL,
            self::SOURCE_YOUTUBE_TAG,
            self::SOURCE_YOUTUBE_PLAYLIST,
        ],
        EnumNetworkType::NETWORK_FACEBOOK => [
            self::SOURCE_FACEBOOK_PAGE,
            self::SOURCE_FACEBOOK_VISITOR_POSTS,
        ],
    ];
}
