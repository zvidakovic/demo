<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumUserTypeType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_user_type_type';

    public const TYPE_FREE = 10;
    public const TYPE_PERSONAL = 20;
    public const TYPE_BUSINESS = 30;
    public const TYPE_PREMIUM = 40;

    public static int $defaultValue = self::TYPE_FREE;

    public static array $valueLabelMap = [
        self::TYPE_FREE => 'Free',
        self::TYPE_PERSONAL => 'Personal',
        self::TYPE_BUSINESS => 'Business',
        self::TYPE_PREMIUM => 'Premium',
    ];
}
