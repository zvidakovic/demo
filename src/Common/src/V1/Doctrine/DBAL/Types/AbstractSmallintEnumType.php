<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Types\Types;
use InvalidArgumentException;

abstract class AbstractSmallintEnumType extends Type
{
    public const NAME = 'enum_abstract';

    public static array $valueLabelMap = [];

    public static int $defaultValue = 0;

    public function getName(): string
    {
        return static::NAME;
    }

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return Types::SMALLINT;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): int
    {
        $value = (int)$value;
        $this->validate($value);
        return $value;
    }

    protected function validate(int $value): void
    {
        if (!array_key_exists($value, static::$valueLabelMap)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid enum "%d" provided in %s.',
                    $value,
                    static::class
                )
            );
        }
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): int
    {
        $value = (int)$value;
        $this->validate($value);
        return $value;
    }
}
