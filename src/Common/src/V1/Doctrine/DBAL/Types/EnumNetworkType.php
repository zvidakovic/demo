<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumNetworkType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_network_type';

    public const NETWORK_TWITTER = 10;
    public const NETWORK_INSTAGRAM = 20;
    public const NETWORK_FACEBOOK = 30;
    public const NETWORK_PINTEREST = 40;
    public const NETWORK_GOOGLE = 50;
    public const NETWORK_VINE = 60;
    public const NETWORK_FLICKR = 70;
    public const NETWORK_YOUTUBE = 80;
    public const NETWORK_TUMBLR = 90;
    public const NETWORK_RSS = 100;
    public const NETWORK_LINKEDIN = 110;
    public const NETWORK_DIFFBOT = 120;
    public const NETWORK_GLASSDOOR = 130;
    public const NETWORK_TIKTOK = 140;

    public static int $defaultValue = self::NETWORK_TWITTER;

    public static array $valueLabelMap = [
        self::NETWORK_TWITTER => 'Twitter',
        self::NETWORK_INSTAGRAM => 'Instagram',
        self::NETWORK_FACEBOOK => 'Facebook',
        self::NETWORK_PINTEREST => 'Pinterest',
        self::NETWORK_GOOGLE => 'Google',
        self::NETWORK_VINE => 'Vine',
        self::NETWORK_FLICKR => 'Flickr',
        self::NETWORK_YOUTUBE => 'YouTube',
        self::NETWORK_TUMBLR => 'Tumblr',
        self::NETWORK_RSS => 'RSS',
        self::NETWORK_LINKEDIN => 'LinkedIn',
        self::NETWORK_DIFFBOT => 'DiffBot',
        self::NETWORK_GLASSDOOR => 'Glassdoor',
        self::NETWORK_TIKTOK => 'TikTok',
    ];
}
