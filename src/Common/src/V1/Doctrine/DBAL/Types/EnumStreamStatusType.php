<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumStreamStatusType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_stream_status_type';

    public const STATUS_NEW = 10;
    public const STATUS_ACTIVE = 20;
    public const STATUS_DISABLED = 30;
    public const STATUS_ARCHIVED = 40;

    public static int $defaultValue = self::STATUS_NEW;

    public static array $valueLabelMap = [
        self::STATUS_NEW => 'New',
        self::STATUS_ACTIVE => 'Confirmed',
        self::STATUS_DISABLED => 'Disabled',
        self::STATUS_ARCHIVED => 'Archived',
    ];
}
