<?php

declare(strict_types=1);

namespace Common\V1\Doctrine\DBAL\Types;

final class EnumUserRoleType extends AbstractSmallintEnumType
{
    public const NAME = 'enum_user_role_type';

    public const ROLE_ADMIN = 10;
    public const ROLE_EDITOR = 20;
    public const ROLE_VIEWER = 30;

    public static int $defaultValue = self::ROLE_ADMIN;

    public static array $valueLabelMap = [
        self::ROLE_ADMIN => 'Admin',
        self::ROLE_EDITOR => 'Editor',
        self::ROLE_VIEWER => 'Viewer',
    ];
}
