<?php

declare(strict_types=1);

namespace Common\V1\Event;

use Common\V1\Entity\User;
use Laminas\EventManager\Event;

final class UserCreatedEvent extends Event
{
    public const NAME = 'user.created';

    protected $name = self::NAME;
    private User $user;

    public function __construct(User $user)
    {
        parent::__construct(self::NAME);
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
