<?php

declare(strict_types=1);

namespace Common\V1;

use Common\V1\Command\GenerateDataCommand;
use Common\V1\Service\LoggerService;
use Laminas\EventManager\EventManagerInterface;
use Laminas\I18n\Translator\Translator;
use Laminas\I18n\Translator\TranslatorInterface;
use Laminas\I18n\Translator\TranslatorServiceFactory;
use Laminas\Mail\Transport\TransportInterface;
use Mezzio\Router\RouterInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

final class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'commands' => $this->getCommands(),
            'hydrators' => $this->getHydrators(),
        ];
    }

    public function getDependencies(): array
    {
        return [
            'invokables' => [
                LoggerService::class => LoggerService::class,
            ],
            'factories' => [
                Middleware\CacheMiddleware::class => Middleware\CacheMiddleware\Factory::class,
                Middleware\SessionLanguageMiddleware::class => Middleware\SessionLanguageMiddleware\Factory::class,
                Translator::class => TranslatorServiceFactory::class,
                Command\GenerateDataCommand::class => Command\GenerateDataCommand\Factory::class,
                CacheInterface::class => Service\CacheService\Factory::class,
                EventManagerInterface::class => Service\EventManagerService\Factory::class,
                RouterInterface::class => Service\FastRouteService\Factory::class,
                Service\UuidService::class => Service\UuidService\Factory::class,
                Service\PasswordService::class => Service\PasswordService\Factory::class,
                Service\JwtService::class => Service\JwtService\Factory::class,
                TransportInterface::class => Service\MailTransportService\Factory::class,
            ],
            'aliases' => [
                TranslatorInterface::class => Translator::class,
                LoggerInterface::class => LoggerService::class,
            ],
        ];
    }

    public function getCommands(): array
    {
        return [
            GenerateDataCommand::class,
        ];
    }

    public function getHydrators(): array
    {
        return [
            'factories' => [
                Hydrator\UserHydrator::class => Hydrator\UserHydrator\Factory::class,
            ],
        ];
    }
}
