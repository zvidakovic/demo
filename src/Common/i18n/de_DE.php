<?php

declare(strict_types=1);

return [
    'english' => 'English',
    'german' => 'Deutsch',
    'help' => 'Hilfe',
    'user' => 'Nutzer',
    'login' => 'Anmelden',
    'logout' => 'Abmelden',
];
