<?php

declare(strict_types=1);

namespace Task\V1\Command;

use Common\V1\Entity\User;
use Doctrine\ORM\EntityManager;
use Event\V1\Service\QueueService;
use Interop\Amqp\AmqpMessage;
use Laminas\Serializer\Serializer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Task\V1\Task\UserCreatedEmailTask;
use Throwable;

final class RunCommand extends Command
{
    protected static $defaultName = 'task:run';

    private EntityManager $entityManager;
    private QueueService $queueService;

    public function __construct(
        EntityManager $entityManager,
        QueueService $queueService
    ) {
        parent::__construct(self::$defaultName);
        $this->entityManager = $entityManager;
        $this->queueService = $queueService;
    }

    public function run(InputInterface $input, OutputInterface $output): int
    {
        $consumer = $this->queueService->getContext()->createConsumer(
            $this->queueService->getContext()->createQueue(UserCreatedEmailTask::class)
        );

        $message = $consumer->receive();
        if (!$message instanceof AmqpMessage) {
            return 0;
        }

        try {
            /** @var User $user */
            $user = Serializer::unserialize($message->getBody());
            //@TODO: Do something with queued task. Send an email.
            $consumer->acknowledge($message);
        } catch (Throwable $error) {
            $consumer->reject($message, true);
        }

        return 0;
    }
}
