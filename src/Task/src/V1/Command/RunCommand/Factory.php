<?php

declare(strict_types=1);

namespace Task\V1\Command\RunCommand;

use Doctrine\ORM\EntityManager;
use Event\V1\Service\QueueService;
use Psr\Container\ContainerInterface;
use Task\V1\Command\RunCommand;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var QueueService $queueService */
        $queueService = $container->get(QueueService::class);

        return new RunCommand(
            $entityManager,
            $queueService
        );
    }
}
