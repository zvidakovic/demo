<?php

declare(strict_types=1);

namespace Task\V1;

use Task\V1\Command\RunCommand;

final class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'commands' => $this->getCommands(),
        ];
    }

    public function getCommands(): array
    {
        return [
            RunCommand::class,
        ];
    }

    public function getDependencies(): array
    {
        return [
            'factories' => [
                Task\UserCreatedEmailTask::class => Task\UserCreatedEmailTask\Factory::class,
                Command\RunCommand::class => Command\RunCommand\Factory::class,
            ],
        ];
    }
}
