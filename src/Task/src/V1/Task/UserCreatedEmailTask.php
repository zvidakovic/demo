<?php

declare(strict_types=1);

namespace Task\V1\Task;

use Amp\Parallel\Worker\Environment;
use Amp\Parallel\Worker\Task;

final class UserCreatedEmailTask implements Task
{
    public function run(Environment $environment): int
    {
        return 0;
    }
}
