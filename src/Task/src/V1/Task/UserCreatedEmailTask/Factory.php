<?php

declare(strict_types=1);

namespace Task\V1\Task\UserCreatedEmailTask;

use Psr\Container\ContainerInterface;
use Task\V1\Task\UserCreatedEmailTask;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        return new UserCreatedEmailTask();
    }
}
