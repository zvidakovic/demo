<?php

declare(strict_types=1);

namespace Api\V1\Common\Delegator\ErrorListener;

use Api\V1\Common\Delegator\ErrorListener;
use Common\V1\Service\LoggerService;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var LoggerService $logger */
        $logger = $container->get(LoggerService::class);

        return new ErrorListener($logger);
    }
}
