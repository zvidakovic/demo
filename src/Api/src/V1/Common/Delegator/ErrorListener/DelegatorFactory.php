<?php

declare(strict_types=1);

namespace Api\V1\Common\Delegator\ErrorListener;

use Api\V1\Common\Delegator\ErrorListener;
use Mezzio\ProblemDetails\ProblemDetailsMiddleware;
use Psr\Container\ContainerInterface;

final class DelegatorFactory
{
    public function __invoke(
        ContainerInterface $container,
        string $name,
        callable $callback
    ): ProblemDetailsMiddleware {
        /** @var ErrorListener $errorListener */
        $errorListener = $container->get(ErrorListener::class);

        /** @var ProblemDetailsMiddleware $errorHandler */
        $errorHandler = $callback();

        $errorHandler->attachListener($errorListener);

        return $errorHandler;
    }
}
