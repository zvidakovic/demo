<?php

declare(strict_types=1);

namespace Api\V1\Common\Delegator;

use Common\V1\Service\LoggerService;
use Laminas\Diactoros\Response;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

final class ErrorListener
{
    private LoggerService $logger;

    public function __construct(
        LoggerService $logger
    ) {
        $this->logger = $logger;
    }

    public function __invoke(
        Throwable $error,
        ServerRequestInterface $request,
        Response $response
    ): Response {
        $this->logger->error($error->getMessage());

        return $response;
    }
}
