<?php

declare(strict_types=1);

namespace Api\V1\Common\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Hydrator\ReflectionHydrator;
use Negotiation\Accept;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

trait ResponseTrait
{
    public function responseFromObject(
        ApiResponseModelInterface $object,
        ServerRequestInterface $request
    ): ResponseInterface {
        //@TODO: Handle XML if needed.
        $accept = new Accept('application/json');

        $hydrator = new ReflectionHydrator();

        $response = new JsonResponse($hydrator->extract($object));

        return $response->withHeader('Content-Type', $accept->getValue());
    }
}
