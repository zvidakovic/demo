<?php

declare(strict_types=1);

namespace Api\V1\Common\Exception;

use Laminas\Json\Json;

abstract class RuntimeException extends \RuntimeException
{
    /** @var array<string,string> */
    protected array $additionalData = [];
    protected string $title = '';
    protected string $detail = '';

    public function getStatus(): int
    {
        return (int)$this->code;
    }

    public function getType(): string
    {
        return sprintf('https://httpstatuses.com/%d', (int)$this->getCode());
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDetail(): string
    {
        return $this->detail;
    }

    /**
     * @return array<string,string>
     */
    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }

    public function jsonSerialize(): string
    {
        return Json::encode($this->toArray());
    }

    /**
     * @return array<string,string>
     */
    public function toArray(): array
    {
        return [];
    }
}
