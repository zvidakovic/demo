<?php

declare(strict_types=1);

namespace Api\V1\Common\Exception;

final class InvalidArgumentsException extends RuntimeException
{
    /**
     * @param string $message
     * @param array<string,string> $additionalData
     */
    public function __construct(string $message, array $additionalData = [])
    {
        parent::__construct($message, 400);
        $this->additionalData = $additionalData;
        $this->title = $this->detail = 'Invalid argument';
    }
}
