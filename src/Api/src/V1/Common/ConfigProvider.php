<?php

declare(strict_types=1);

namespace Api\V1\Common;

use Mezzio\ProblemDetails\ProblemDetailsMiddleware;

final class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'commands' => $this->getCommands(),
        ];
    }

    public function getDependencies(): array
    {
        return [
            'factories' => [
                Delegator\ErrorListener::class => Delegator\ErrorListener\Factory::class,
            ],
            'delegators' => [
                ProblemDetailsMiddleware::class => [
                    Delegator\ErrorListener\DelegatorFactory::class,
                ],
            ],
        ];
    }

    public function getCommands(): array
    {
        return [];
    }
}
