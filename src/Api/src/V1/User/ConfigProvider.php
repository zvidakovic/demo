<?php

declare(strict_types=1);

namespace Api\V1\User;

use Api\V1\User\Collection\UserCollection;
use Common\V1\Entity\User;
use Common\V1\Hydrator\UserHydrator;
use Common\V1\Service\UuidService;
use Fig\Http\Message\RequestMethodInterface;
use Mezzio\Hal\Metadata\MetadataMap;
use Mezzio\Hal\Metadata\RouteBasedCollectionMetadata;
use Mezzio\Hal\Metadata\RouteBasedResourceMetadata;

final class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'router' => $this->getRoutes(),
            MetadataMap::class => [
                [
                    '__class__' => RouteBasedResourceMetadata::class,
                    'resource_class' => User::class,
                    'resource_identifier' => 'uuid',
                    'route_identifier_placeholder' => 'uuid',
                    'route' => 'api.v1.user.get',
                    'extractor' => UserHydrator::class,
                ],
                [
                    '__class__' => RouteBasedCollectionMetadata::class,
                    'collection_class' => UserCollection::class,
                    'collection_relation' => 'user',
                    'route' => 'api.v1.users.get',
                ],
            ],
        ];
    }

    public function getDependencies(): array
    {
        return [
            'factories' => [
                Handler\UserGetHandler::class => Handler\UserGetHandler\Factory::class,
                Handler\UsersGetHandler::class => Handler\UsersGetHandler\Factory::class,
                Handler\UserDeleteHandler::class => Handler\UserDeleteHandler\Factory::class,
                Handler\LoginHandler::class => Handler\LoginHandler\Factory::class,
                Handler\RegisterHandler::class => Handler\RegisterHandler\Factory::class,
                Service\UserCreateService::class => Service\UserCreateService\Factory::class,
                Service\LoginService::class => Service\LoginService\Factory::class,
            ],
        ];
    }

    public function getRoutes(): array
    {
        return [
            'routes' => [
                [
                    'name' => 'api.v1.login.post',
                    'path' => '/v1/login',
                    'middleware' => [
                        Handler\LoginHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_POST],
                ],
                [
                    'name' => 'api.v1.register.post',
                    'path' => '/v1/register',
                    'middleware' => [
                        Handler\RegisterHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_POST],
                ],
                [
                    'name' => 'api.v1.logout.post',
                    'path' => '/v1/logout',
                    'middleware' => [
                        Handler\LoginHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_POST],
                ],
                [
                    'name' => 'api.v1.users.get',
                    'path' => '/v1/users',
                    'middleware' => [
                        Handler\UsersGetHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_GET],
                ],
                [
                    'name' => 'api.v1.user.get',
                    'path' => sprintf('/v1/user/{uuid:%s}', UuidService::FASTROUTE_UUID_REGEX),
                    'middleware' => [
                        Handler\UserGetHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_GET],
                ],
                [
                    'name' => 'api.v1.user.delete',
                    'path' => sprintf('/v1/user/{uuid:%s}', UuidService::FASTROUTE_UUID_REGEX),
                    'middleware' => [
                        Handler\UserDeleteHandler::class,
                    ],
                    'methods' => [RequestMethodInterface::METHOD_DELETE],
                ],
            ],
        ];
    }
}
