<?php

declare(strict_types=1);

namespace Api\V1\User\Handler;

use Api\V1\Common\Handler\ResponseTrait;
use Api\V1\User\Handler\LoginHandler\Request\LoginRequest;
use Api\V1\User\Handler\LoginHandler\Response\LoginResponse;
use Api\V1\User\Service\LoginService;
use Common\V1\Entity\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class LoginHandler implements RequestHandlerInterface
{
    use ResponseTrait;

    private LoginService $loginService;

    public function __construct(
        LoginService $loginService
    ) {
        $this->loginService = $loginService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var array<string,string> $data */
        $data = (array)$request->getParsedBody();

        $requestModel = new LoginRequest();
        $requestModel->setData($data);

        if (!$requestModel->isValid()) {
            return $this->responseFromObject(
                new LoginResponse(false, '', (array)$requestModel->getMessages()),
                $request
            );
        }

        /** @var array<string,string> $data */
        $data = $requestModel->getData();
        $user = $this->loginService->getUserByEmail($data['email']);

        if (!$user instanceof User) {
            return $this->responseFromObject(new LoginResponse(false, '', ['user' => 'User not found']), $request);
        }

        return $this->responseFromObject(
            new LoginResponse(
                true,
                $this->loginService->getNewSessionJwtToken($user),
                []
            ),
            $request
        );
    }
}
