<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LogoutHandler\Request;

use Api\V1\Common\Handler\ApiRequestModelInterface;
use Laminas\Form\Element\Email;
use Laminas\Form\Element\Password;
use Laminas\Form\Form;

final class LogoutRequest extends Form implements ApiRequestModelInterface
{
    public function __construct()
    {
        parent::__construct('loginRequest', []);
        $this->add(new Email('email', ['label' => 'Email']));
        $this->add(new Password('password', ['label' => 'Password']));
    }
}
