<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LogoutHandler;

use Api\V1\User\Handler\LogoutHandler;
use Common\V1\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Laminas\Diactoros\ResponseFactory;
use Mezzio\Hal\HalResponseFactory;
use Mezzio\Hal\ResourceGenerator;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var EntityRepository $repository */
        $repository = $entityManager->getRepository(User::class);
        /** @var ResourceGenerator $resourceGenerator */
        $resourceGenerator = $container->get(ResourceGenerator::class);
        /** @var HalResponseFactory $halResponseFactory */
        $halResponseFactory = $container->get(HalResponseFactory::class);
        /** @var ResponseFactory $responseFactory */
        $responseFactory = $container->get(ResponseFactory::class);

        return new LogoutHandler(
            $repository,
            $resourceGenerator,
            $halResponseFactory,
            $responseFactory
        );
    }
}
