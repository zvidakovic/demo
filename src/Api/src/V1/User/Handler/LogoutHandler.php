<?php

declare(strict_types=1);

namespace Api\V1\User\Handler;

use Api\V1\Common\Handler\ResponseTrait;
use Api\V1\User\Handler\LoginHandler\Request\LoginRequest;
use Api\V1\User\Handler\LoginHandler\Response\LoginResponse;
use Doctrine\ORM\EntityRepository;
use Laminas\Diactoros\ResponseFactory;
use Mezzio\Hal\HalResponseFactory;
use Mezzio\Hal\ResourceGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class LogoutHandler implements RequestHandlerInterface
{
    use ResponseTrait;

    private EntityRepository $entityRepository;
    private ResourceGenerator $resourceGenerator;
    private HalResponseFactory $halResponseFactory;
    private ResponseFactory $responseFactory;

    public function __construct(
        EntityRepository $entityRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $halResponseFactory,
        ResponseFactory $responseFactory
    ) {
        $this->entityRepository = $entityRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->halResponseFactory = $halResponseFactory;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var array<string,string> $data */
        $data = (array)$request->getParsedBody();

        $loginRequest = new LoginRequest();
        $loginRequest->setData($data);

        if (!$loginRequest->isValid()) {
            return $this->responseFromObject(
                new LoginResponse(false, '', (array)$loginRequest->getMessages()),
                $request
            );
        }

        return $this->responseFromObject(
            new LoginResponse(true, '', []),
            $request
        );
    }
}
