<?php

declare(strict_types=1);

namespace Api\V1\User\Handler;

use Api\V1\User\Collection\UserCollection;
use Common\V1\Hal\ResourceGenerator\UserResourceStrategy;
use Doctrine\ORM\EntityRepository;
use Mezzio\Hal\HalResponseFactory;
use Mezzio\Hal\Metadata\RouteBasedResourceMetadata;
use Mezzio\Hal\ResourceGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class UsersGetHandler implements RequestHandlerInterface
{
    private EntityRepository $entityRepository;
    private ResourceGenerator $resourceGenerator;
    private HalResponseFactory $halResponseFactory;

    public function __construct(
        EntityRepository $entityRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $halResponseFactory
    ) {
        $this->entityRepository = $entityRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->halResponseFactory = $halResponseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $query = $this->entityRepository
            ->createQueryBuilder('u')
            ->getQuery()
            ->setMaxResults(20);

        $paginator = new UserCollection($query);

        $this->resourceGenerator->addStrategy(
            RouteBasedResourceMetadata::class,
            UserResourceStrategy::class
        );

        $resource = $this->resourceGenerator->fromObject($paginator, $request);

        return $this->halResponseFactory->createResponse($request, $resource);
    }
}
