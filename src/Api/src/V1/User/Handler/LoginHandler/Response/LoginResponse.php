<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LoginHandler\Response;

use Api\V1\Common\Handler\ApiResponseModelInterface;

final class LoginResponse implements ApiResponseModelInterface
{
    private bool $success;
    private array $errors;
    private string $jwt;

    public function __construct(
        bool $success = false,
        string $jwt = '',
        array $errors = []
    ) {
        $this->success = $success;
        $this->jwt = $jwt;
        $this->errors = $errors;
    }
}
