<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LoginHandler;

use Api\V1\User\Handler\LoginHandler;
use Api\V1\User\Service\LoginService;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var LoginService $loginService */
        $loginService = $container->get(LoginService::class);

        return new LoginHandler(
            $loginService
        );
    }
}
