<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LoginHandler\Request\LoginRequest;

use Laminas\Filter\Boolean;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Checkbox;
use Laminas\InputFilter\InputFilterProviderInterface;

final class RememberMeElement extends Checkbox implements InputFilterProviderInterface
{
    public const NAME = 'rememberMe';

    public function __construct()
    {
        parent::__construct(
            self::NAME,
            [
                'label' => 'Remember me',
            ]
        );
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name' => self::NAME,
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => Boolean::class],
            ],
        ];
    }
}
