<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LoginHandler\Request;

use Api\V1\Common\Handler\ApiRequestModelInterface;
use Laminas\Form\Element;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

final class LoginRequest extends Form implements ApiRequestModelInterface
{
    public const NAME = 'loginRequest';

    public function __construct()
    {
        parent::__construct(self::NAME, []);
        $this->add(new LoginRequest\EmailElement());
        $this->add(new LoginRequest\PasswordElement());
        $this->add(new LoginRequest\RememberMeElement());
    }

    public function getInputFilterSpecification(): array
    {
        /** @var Element[]&InputFilterProviderInterface[] $elements */
        $elements = $this->getElements();

        return [
            $elements[LoginRequest\EmailElement::NAME]->getInputFilterSpecification(),
            $elements[LoginRequest\PasswordElement::NAME]->getInputFilterSpecification(),
            $elements[LoginRequest\RememberMeElement::NAME]->getInputFilterSpecification(),
        ];
    }
}
