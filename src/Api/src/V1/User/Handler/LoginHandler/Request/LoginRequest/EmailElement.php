<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\LoginHandler\Request\LoginRequest;

use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Text;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\NotEmpty;

final class EmailElement extends Text implements InputFilterProviderInterface
{
    public const NAME = 'email';

    public function __construct()
    {
        parent::__construct(
            self::NAME,
            [
                'label' => 'Email',
            ]
        );
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name' => self::NAME,
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => NotEmpty::class],
                ['name' => EmailAddress::class],
            ],
        ];
    }
}
