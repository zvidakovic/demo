<?php

declare(strict_types=1);

namespace Api\V1\User\Handler;

use Api\V1\Common\Exception\InvalidArgumentsException;
use Api\V1\Common\Exception\ResourceNotFoundException;
use Common\V1\Entity\User;
use Doctrine\ORM\EntityRepository;
use Mezzio\Hal\HalResponseFactory;
use Mezzio\Hal\ResourceGenerator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Ramsey\Uuid\Uuid;
use Throwable;

final class UserDeleteHandler implements RequestHandlerInterface
{
    private EntityRepository $entityRepository;
    private ResourceGenerator $resourceGenerator;
    private HalResponseFactory $halResponseFactory;

    public function __construct(
        EntityRepository $entityRepository,
        ResourceGenerator $resourceGenerator,
        HalResponseFactory $halResponseFactory
    ) {
        $this->entityRepository = $entityRepository;
        $this->resourceGenerator = $resourceGenerator;
        $this->halResponseFactory = $halResponseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $userUuid = (string)$request->getAttribute('uuid', '');

        if ($userUuid === '') {
            throw new InvalidArgumentsException('User UUID is not provided.');
        }

        try {
            $userUuid = Uuid::fromString($userUuid);
        } catch (Throwable $error) {
            throw new InvalidArgumentsException('Invalid UUID provided.');
        }

        /** @var User $entity */
        $entity = $this->entityRepository->find($userUuid);

        if (!$entity instanceof User) {
            throw new ResourceNotFoundException('User not found.');
        }

        $resource = $this->resourceGenerator->fromObject($entity, $request);

        return $this->halResponseFactory->createResponse($request, $resource);
    }
}
