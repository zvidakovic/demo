<?php

declare(strict_types=1);

namespace Api\V1\User\Handler;

use Api\V1\Common\Handler\ResponseTrait;
use Api\V1\User\Handler\RegisterHandler\Request\RegisterRequest;
use Api\V1\User\Handler\RegisterHandler\Response\RegisterResponse;
use Api\V1\User\Service\UserCreateService;
use Common\V1\Entity\User;
use Doctrine\ORM\EntityRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class RegisterHandler implements RequestHandlerInterface
{
    use ResponseTrait;

    private EntityRepository $entityRepository;
    private UserCreateService $userCreateService;

    public function __construct(
        EntityRepository $entityRepository,
        UserCreateService $userCreateService
    ) {
        $this->entityRepository = $entityRepository;
        $this->userCreateService = $userCreateService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var array<string,string> $data */
        $data = (array)$request->getParsedBody();

        $requestModel = new RegisterRequest();
        $requestModel->setData($data);

        if (!$requestModel->isValid()) {
            return $this->responseFromObject(
                new RegisterResponse(false, (array)$requestModel->getMessages()),
                $request
            );
        }

        /** @var array<string,string> $data */
        $data = $requestModel->getData();

        /** @var User|null $entity */
        $entity = $this->entityRepository->findOneBy(['email' => (string)$data['email']]);
        if ($entity instanceof User) {
            return $this->responseFromObject(
                new RegisterResponse(false, ['user' => ['user' => 'User already exists']]),
                $request
            );
        }

        $this->userCreateService->createUserFromRequestModel($requestModel);

        return $this->responseFromObject(
            new RegisterResponse(true, []),
            $request
        );
    }
}
