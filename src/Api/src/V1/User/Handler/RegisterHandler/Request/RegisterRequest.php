<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler\Request;

use Laminas\Form\Element;
use Laminas\Form\Form;
use Laminas\InputFilter\InputFilterProviderInterface;

final class RegisterRequest extends Form implements InputFilterProviderInterface
{
    public const NAME = 'registerRequest';

    public function __construct()
    {
        parent::__construct(self::NAME, ['label' => 'Register']);
        $this->add(new RegisterRequest\FirstNameElement());
        $this->add(new RegisterRequest\LastNameElement());
        $this->add(new RegisterRequest\EmailElement());
        $this->add(new RegisterRequest\PasswordElement());
        $this->add(new RegisterRequest\PasswordRepeatElement());
        $this->add(new RegisterRequest\TypeElement());
    }

    public function getInputFilterSpecification(): array
    {
        /** @var Element[]&InputFilterProviderInterface[] $elements */
        $elements = $this->getElements();

        return [
            $elements[RegisterRequest\FirstNameElement::NAME]->getInputFilterSpecification(),
            $elements[RegisterRequest\LastNameElement::NAME]->getInputFilterSpecification(),
            $elements[RegisterRequest\EmailElement::NAME]->getInputFilterSpecification(),
            $elements[RegisterRequest\PasswordElement::NAME]->getInputFilterSpecification(),
            $elements[RegisterRequest\PasswordRepeatElement::NAME]->getInputFilterSpecification(),
            $elements[RegisterRequest\TypeElement::NAME]->getInputFilterSpecification(),
        ];
    }
}
