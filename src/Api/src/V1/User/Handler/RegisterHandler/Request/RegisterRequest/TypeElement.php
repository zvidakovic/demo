<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler\Request\RegisterRequest;

use Common\V1\Doctrine\DBAL\Types\EnumUserTypeType;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToInt;
use Laminas\Form\Element\Select;
use Laminas\I18n\Validator\IsInt;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Callback;
use Laminas\Validator\NotEmpty;

final class TypeElement extends Select implements InputFilterProviderInterface
{
    public const NAME = 'type';

    public function __construct()
    {
        parent::__construct(
            self::NAME,
            [
                'label' => 'Type',
                'value_options' => EnumUserTypeType::$valueLabelMap,
            ]
        );
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name' => self::NAME,
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => ToInt::class],
            ],
            'validators' => [
                ['name' => NotEmpty::class],
                ['name' => IsInt::class],
                [
                    'name' => Callback::class,
                    'options' => [
                        'callback' => static function ($value, array $context): bool {
                            return array_key_exists($value, EnumUserTypeType::$valueLabelMap);
                        },
                    ],
                ],
            ],
        ];
    }
}
