<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler\Request\RegisterRequest;

use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Text;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;

final class FirstNameElement extends Text implements InputFilterProviderInterface
{
    public const NAME = 'firstName';

    public function __construct()
    {
        parent::__construct(
            self::NAME,
            [
                'label' => 'First name',
            ]
        );
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name' => self::NAME,
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => NotEmpty::class],
                ['name' => StringLength::class, 'options' => ['min' => 3, 'max' => 255]],
            ],
        ];
    }
}
