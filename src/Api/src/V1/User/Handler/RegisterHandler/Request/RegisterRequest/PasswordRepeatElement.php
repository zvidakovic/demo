<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler\Request\RegisterRequest;

use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Password;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Callback;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\StringLength;

final class PasswordRepeatElement extends Password implements InputFilterProviderInterface
{
    public const NAME = 'passwordRepeat';

    public function __construct()
    {
        parent::__construct(
            self::NAME,
            [
                'label' => 'Repeat password',
            ]
        );
    }

    public function getInputFilterSpecification(): array
    {
        return [
            'name' => self::NAME,
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                ['name' => NotEmpty::class],
                ['name' => StringLength::class, 'options' => ['min' => 5]],
                [
                    'name' => Callback::class,
                    'options' => [
                        'callback' => static function ($value, array $context): bool {
                            if (!isset($context['password'])) {
                                return false;
                            }
                            return $value === $context['password'];
                        },
                    ],
                ],
            ],
        ];
    }
}
