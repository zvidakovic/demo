<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler\Response;

use Api\V1\Common\Handler\ApiResponseModelInterface;

final class RegisterResponse implements ApiResponseModelInterface
{
    private bool $success;
    private array $errors;

    public function __construct(
        bool $success = false,
        array $errors = []
    ) {
        $this->success = $success;
        $this->errors = $errors;
    }
}
