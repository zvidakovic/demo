<?php

declare(strict_types=1);

namespace Api\V1\User\Handler\RegisterHandler;

use Api\V1\User\Handler\RegisterHandler;
use Api\V1\User\Service\UserCreateService;
use Common\V1\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get(EntityManager::class);
        /** @var EntityRepository $repository */
        $repository = $entityManager->getRepository(User::class);
        /** @var UserCreateService $userCreateService */
        $userCreateService = $container->get(UserCreateService::class);

        return new RegisterHandler(
            $repository,
            $userCreateService
        );
    }
}
