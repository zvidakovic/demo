<?php

declare(strict_types=1);

namespace Api\V1\User\Service\UserCreateService;

use Api\V1\User\Service\UserCreateService;
use Common\V1\Hydrator\UserHydrator;
use Common\V1\Service\PasswordService;
use Common\V1\Service\UuidService;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Hydrator\HydratorPluginManager;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var PasswordService $passwordService */
        $passwordService = $container->get(PasswordService::class);

        /** @var UuidService $uuidService */
        $uuidService = $container->get(UuidService::class);

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get(EntityManager::class);

        /** @var EventManagerInterface $eventManager */
        $eventManager = $container->get(EventManagerInterface::class);

        /** @var HydratorPluginManager $hydratorPluginManager */
        $hydratorPluginManager = $container->get(HydratorPluginManager::class);

        /** @var DoctrineObject $hydrator */
        $hydrator = $hydratorPluginManager->get(UserHydrator::class);

        return new UserCreateService(
            $hydrator,
            $passwordService,
            $uuidService,
            $entityManager,
            $eventManager,
        );
    }
}
