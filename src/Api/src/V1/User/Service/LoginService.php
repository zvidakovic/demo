<?php

declare(strict_types=1);

namespace Api\V1\User\Service;

use Common\V1\Doctrine\DBAL\Types\EnumUserStatusType;
use Common\V1\Entity\User;
use Common\V1\Service\JwtService;
use DateInterval;
use DateTimeImmutable;
use Doctrine\Persistence\ObjectRepository;
use Laminas\EventManager\EventManagerInterface;

final class LoginService
{
    private JwtService $jwtService;
    private ObjectRepository $userRepository;
    private EventManagerInterface $eventManager;

    public function __construct(
        JwtService $jwtService,
        ObjectRepository $userRepository,
        EventManagerInterface $eventManager
    ) {
        $this->jwtService = $jwtService;
        $this->userRepository = $userRepository;
        $this->eventManager = $eventManager;
    }

    public function getNewSessionJwtToken(User $user): string
    {
        return $this->jwtService->encode(
            [
                'publicUuid' => $user->getPublicUuid()->toString(),
                'iat' => (new DateTimeImmutable())->add(new DateInterval('P1D'))->format('u'),
            ]
        );
    }

    public function getUserByEmail(string $email): ?User
    {
        /** @var User|null $user */
        $user = $this->userRepository->findOneBy(
            [
                'email' => $email,
                'status' => EnumUserStatusType::STATUS_CONFIRMED,
            ]
        );
        return $user;
    }
}
