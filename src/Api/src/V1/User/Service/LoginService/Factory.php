<?php

declare(strict_types=1);

namespace Api\V1\User\Service\LoginService;

use Api\V1\User\Service\LoginService;
use Common\V1\Entity\User;
use Common\V1\Service\JwtService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\EventManager\EventManagerInterface;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get(EntityManager::class);
        $userRepository = $entityManager->getRepository(User::class);

        /** @var JwtService $jwtRepositor */
        $jwtService = $container->get(JwtService::class);

        /** @var EventManagerInterface $eventManager */
        $eventManager = $container->get(EventManagerInterface::class);

        return new LoginService(
            $jwtService,
            $userRepository,
            $eventManager,
        );
    }
}
