<?php

declare(strict_types=1);

namespace Api\V1\User\Service;

use Api\V1\User\Handler\RegisterHandler\Request\RegisterRequest;
use Common\V1\Entity\User;
use Common\V1\Event\UserCreatedEvent;
use Common\V1\Service\PasswordService;
use Common\V1\Service\UuidService;
use DateTimeImmutable;
use Doctrine\Laminas\Hydrator\DoctrineObject;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\EventManager\EventManagerInterface;

final class UserCreateService
{
    private DoctrineObject $hydrator;
    private PasswordService $passwordService;
    private UuidService $uuidService;
    private EntityManagerInterface $entityManager;
    private EventManagerInterface $eventManager;

    public function __construct(
        DoctrineObject $hydrator,
        PasswordService $passwordService,
        UuidService $uuidService,
        EntityManagerInterface $entityManager,
        EventManagerInterface $eventManager
    ) {
        $this->hydrator = $hydrator;
        $this->passwordService = $passwordService;
        $this->uuidService = $uuidService;
        $this->entityManager = $entityManager;
        $this->eventManager = $eventManager;
    }

    public function createUserFromRequestModel(RegisterRequest $registerRequest): User
    {
        /** @var array<string,string> $data */
        $data = (array)$registerRequest->getData();

        /** @var User $entity */
        $entity = $this->hydrator->hydrate($data, new User());

        $entity->setPublicUuid($this->uuidService->generateUuid($entity->getEmail()));
        $entity->setPasswordHash($this->passwordService->generatePassword($data['password']));

        $now = new DateTimeImmutable();
        $entity->setDateUpdated($now);
        $entity->setDateCreated($now);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->eventManager->triggerEvent(new UserCreatedEvent($entity));

        return $entity;
    }
}
