<?php

declare(strict_types=1);

namespace Event\V1\Listener;

use Common\V1\Event\UserCreatedEvent;
use Common\V1\Service\LoggerService;
use Event\V1\Service\QueueService;
use Laminas\EventManager\EventManager;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerInterface;

final class UserCreatedListener implements EventManagerAwareInterface
{
    private LoggerService $logger;
    private EventManagerInterface $eventManager;
    private QueueService $queueService;

    public function __construct(
        LoggerService $logger,
        QueueService $queueService
    ) {
        $this->logger = $logger;
        $this->queueService = $queueService;
    }

    public function getEventManager(): EventManagerInterface
    {
        if (!isset($this->eventManager)) {
            $this->eventManager = new EventManager();
        }
        return $this->eventManager;
    }

    public function setEventManager(EventManagerInterface $eventManager): void
    {
        $this->eventManager = $eventManager;
        $this->eventManager->attach(UserCreatedEvent::NAME, [$this, 'onUserCreate']);
    }

    public function onUserCreate(UserCreatedEvent $event): void
    {
        $this->queueService->pushUserCreatedEmailTaskQueue($event->getUser());
    }
}
