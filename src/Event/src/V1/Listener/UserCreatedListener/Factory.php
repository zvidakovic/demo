<?php

declare(strict_types=1);

namespace Event\V1\Listener\UserCreatedListener;

use Common\V1\Service\LoggerService;
use Event\V1\Listener\UserCreatedListener;
use Event\V1\Service\QueueService;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var LoggerService $logger */
        $logger = $container->get(LoggerService::class);
        /** @var QueueService $queueService */
        $queueService = $container->get(QueueService::class);

        return new UserCreatedListener(
            $logger,
            $queueService
        );
    }
}
