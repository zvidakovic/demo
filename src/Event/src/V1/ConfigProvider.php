<?php

declare(strict_types=1);

namespace Event\V1;

final class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    public function getDependencies(): array
    {
        return [
            'factories' => [
                Listener\UserCreatedListener::class => Listener\UserCreatedListener\Factory::class,
                Service\QueueService::class => Service\QueueService\Factory::class,
            ],
        ];
    }
}
