<?php

declare(strict_types=1);

namespace Event\V1\Service;

use Common\V1\Entity\User;
use Common\V1\Service\LoggerService;
use Interop\Amqp\AmqpContext;
use Interop\Amqp\AmqpQueue;
use Interop\Amqp\AmqpTopic;
use Laminas\Serializer\Serializer;
use Task\V1\Task\UserCreatedEmailTask;
use Throwable;

final class QueueService
{
    private LoggerService $logger;
    private AmqpContext $amqpContext;

    public function __construct(
        LoggerService $logger,
        AmqpContext $amqpContext
    ) {
        $this->logger = $logger;
        $this->amqpContext = $amqpContext;
    }

    public function getContext(): AmqpContext
    {
        return $this->amqpContext;
    }

    public function pushUserCreatedEmailTaskQueue(User $user): void
    {
        try {
            $this->createQueue(UserCreatedEmailTask::class);
            $this->amqpContext->createProducer()->send(
                $this->amqpContext->createQueue(UserCreatedEmailTask::class),
                $this->amqpContext->createMessage(Serializer::serialize($user))
            );
        } catch (Throwable $error) {
            $this->logger->error($error->getMessage());
        }
    }

    private function createTopic(string $topicName): AmqpTopic
    {
        $topic = $this->amqpContext->createTopic($topicName);
        $this->amqpContext->declareTopic($topic);
        return $topic;
    }

    private function createQueue(string $queueName): AmqpQueue
    {
        $queue = $this->amqpContext->createQueue($queueName);
        $this->amqpContext->declareQueue($queue);
        return $queue;
    }
}
