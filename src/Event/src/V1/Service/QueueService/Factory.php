<?php

declare(strict_types=1);

namespace Event\V1\Service\QueueService;

use Common\V1\Service\LoggerService;
use Enqueue\AmqpBunny\AmqpConnectionFactory;
use Event\V1\Service\QueueService;
use Psr\Container\ContainerInterface;

final class Factory
{
    public function __invoke(ContainerInterface $container)
    {
        /** @var array[] $config */
        $config = $container->get('config');
        /** @var LoggerService $logger */
        $logger = $container->get(LoggerService::class);

        return new QueueService(
            $logger,
            (new AmqpConnectionFactory($config[AmqpConnectionFactory::class]))->createContext()
        );
    }
}
