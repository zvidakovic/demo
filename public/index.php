<?php

declare(strict_types=1);

use Mezzio\Application;
use Mezzio\MiddlewareFactory;
use Psr\Container\ContainerInterface;
use Symfony\Component\Dotenv\Dotenv;

if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

require dirname(__DIR__) . '/vendor/autoload.php';
(new Dotenv())->loadEnv(dirname(__DIR__) . '/config/.env');

(static function () {
    /** @var ContainerInterface $container */
    $container = require dirname(__DIR__) . '/config/container.php';

    /** @var Application $application */
    $application = $container->get(Application::class);

    /** @var MiddlewareFactory $middlewareFactory */
    $middlewareFactory = $container->get(MiddlewareFactory::class);

    (require dirname(__DIR__) . '/config/pipeline.php')($application, $middlewareFactory, $container);
    (require dirname(__DIR__) . '/config/routes.php')($application, $middlewareFactory, $container);

    $application->run();
})();
